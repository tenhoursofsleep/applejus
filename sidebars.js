module.exports = {
  mainSidebar: {
    "GK Öffentliches Recht II": [
      'oer0',
      {
      "§ 1 Allgemeines": ['oer-allgemeines-verfassungsbeschwerde', "oer1-2", "oer1-3", "oer1-4", "oer1-5", "oer1-6", "oer-allgemeines-drittwirkung"]
      },
      {
        "§ 2 Menschenwürde": ['oer2-1', "oer2-2", "oer2-3"],
      },
      {
        "§ 3 Allg. Handlungsfreiheit": ['oer-handlungsfreiheit-1']
      },
      {
        "§ 6 Gleichheit": ['oer-gleichheit-1']
      },
      {
        "§ 8 Meinungsfreiheit": ['oer-meinungsfreiheit-1', 'oer-meinungsfreiheit-schranke', "oer-meinungsfreiheit-schrankenschranke"]
      },
      {
        "§ 10 Versammlungsfreiheit": ['oer-versammlungsfreiheit-1', 'oer-versammlungsfreiheit-2', 'oer-versammlungsfreiheit-3']
      },
      {
        "§ 12 Berufsfreiheit": ['oer-berufsfreiheit-1', "oer-berufsfreiheit-2", "oer-berufsfreiheit-3"]
      }
    ],
    "GK Zivilrecht": [
      {
        "Schuldrecht AT": ["zr-srat-1", "zr-srat-278", "zr-srat-285", "zr-srat-dsl", "zr-srat-823"],
        "Kaufrecht": ["zr-kr-1", "zr-kr-rücktritt", "zr-kr-schadensersatz", "zr-kr-mangel"],
        "Werkvertragsrecht": [],
        "Dienstvertragsrecht": [],
        "Mietrecht": [],
      }
    ],
    "Style Guide" : ['doc1', 'doc2', 'doc3', 'mdx'],
  }
};
