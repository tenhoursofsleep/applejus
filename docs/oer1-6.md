---
id: oer1-6
title: VI. Verfassungsmäßigkeit eines Gesetzes
sidebar_label: VI. Verfassungsmäßigkeit eines Gesetzes
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer1-5.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer1-5
---
Teil der *"Schranken-Schranken"* - Verfassungsrechtliche Grenzen der Einschränkbarkeit durch Grundrechtsschranken.

1. Formelle Verfassungsmäßigkeit
   1. Kompetenz
   2. Verfahren
   3. Form
2. Materielle Verfassungsmäßigkeit
   1. Verhältnismäßigkeit
   2. Verbot des Einzelfallgesetzes
   3. Zitiergebot
   4. Verletzung des Wesensgehaltes



### Quellen
[^1]: Fall 1, GK ÖR SS 2020 
[^2]: Classen, Grundrechte 
[^3]: Fall 3, GK ÖR SS 2020 
[^4]: Skript 