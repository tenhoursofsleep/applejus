---
id: oer-handlungsfreiheit-1
title: I. Schutzbereich
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-handlungsfreiheit-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-handlungsfreiheit-1
---

### Sachlicher Schutzbereich

Es war (zumindest) früher umstritten, ob das "Recht auf die freie Entfaltung" der Persönlichkeit aus Art. 2 I 1 GG nur die Entfaltung innerhalb eines **Kernbereichs** der Persönlichkeit, "der das Wesen des Menschen als geistig-sittliche Person ausmacht"[^Elfes] schützt, oder ob es im Sinne einer **allgemeinen Handlungsfreiheit** auch banale Handlungen umfasst. Aufgrund der **weiten Schrankenregelung** des Grundrechts, und mit Hinblick auf die ursprüngliche, aus sprachlichen Gründen verworfenen Formulierung dieses Artikels: `"Jeder kann tun und lassen, was er will"`, ist heute allgemein die zweite Ansicht eines **entsprechend weiten Verständnis des Schutzbereichs** vertreten.

> Es handelt sich bei der allg. Handlungsfreiheit um ein **Auffanggrundrecht**, das die prinzipielle Freiheit eines Menschen, nach seinem Belieben zu handeln und zu unterlassen, schützt. Es kommt immer dann in Betracht, wenn der Schutzbereich keines anderen Grundrechts **eröffnet** ist.

Es haben sich in der Rspr. viele Fallgruppen gebildet, darunter:

- das Reiten im Walde
- die Vertragsfreiheit im privatrechtlichen Sinne
- Alkoholkonsum
- Fahren ohne Sicherheitsgurt.

:::note Hinweis

Als Auffanggrundrecht ist der Schutzbereich der allg. Handlungsfreiheit schon dann nicht eröffnet, wenn der **Schutzbereich** eines anderen Grundrechts eröffnet ist -- auch wenn dort am Ende keine Verletzung vorliegt[^Skript].

:::

### Persönlicher Schutzbereich

Zumindest alle natürliche Personen. Ob auch juristische Personen geschützt sein können? I think so.




### Quellen

[^Skript]: Skript 

[^Elfes]: Elfes-Entscheidung, BVerfGE 6, 32

[^mMer]: Meine Meinung :)