---
id: oer-meinungsfreiheit-schranke
title: II. Schranke
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-meinungsfreiheit-schranke/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-meinungsfreiheit-schranke
---



Die Meinungsfreiheit unterliegt einem qualifizierten Gesetzesvorbehalt:

- allgemeine Gesetze
- gesetzliche Bestimmungen zum Schutze der Jugend
- Recht der persönlichen Ehre



### Allgemeines Gesetz

:::note Definition

**Allgemeine Gesetze** sind alle Gesetze im **materiellen** Sinne, die sich nicht gegen eine Meinung als solche richten, sondern vielmehr dem Schutz eines **schlechthin**, ohne Rücksicht auf eine bestimmte Meinung zu schützenden Rechtsgut dienen. Dieses Rechtsgut muss in der Rechtsordnung allgemein und damit unabhängig davon geschützt sein, ob es durch Meinungsäußerungen oder auf andere Weise verletzt werden kann.

:::


### Recht der persönlichen Ehre

Teil des allgemeinen Persönlichkeitsrecht und genießt selbst grundrechtlichen Schutz. Jedoch heute nicht von eigenständiger Bedeutung, da bestehende Ehrenschutzbestimmungen auch alle allgemeine Gesetze sind: zB §§ 185 ff. StGB i.V.m. §§ 823, 1004 BGB.

BVerfG sogar: die Gesetze hier **müssen** auch allgemeine Gesetze sein.




## Quellen

Fall 5

