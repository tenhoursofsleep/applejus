---
id: oer1-5
title: V. Die Verhältnismäßigkeit
sidebar_label: V. Verhältnismäßigkeit
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer1-5.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer1-5
---
Herleitung aus Art. 20 Abs 1.

Aufbau:

#### 1. Legitimer Zweck

#### 2. Geeignetheit

#### 3. Erforderlichkeit

#### 4. Angemessenheit

Hier wird meistens der Schwerpunkt sein. Es bietet sich folgender Aufbau an:

##### 	a. Schwere des Eingriffs

##### 	b. Gewicht der entgegenstehende Ziele

##### 	c. Abwägung

 