---
id: zr-srat-278
sidebar_label: Haftung für Erfüllungsgehilfe, 278
title: Haftung für den Erfüllungsgehilfen
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-srat-278.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-srat-278
---



### Aufbau

Unter **Vertretenmüssen**

1. Eigenes Verschulden des Schuldners (-)

2. Zurechnung des Verschuldens des Erfüllungsgehilfen, § 278 BGB

   1. Sonderrechtsbeziehung Geschädigter -- Schuldner

      > § 278 BGB ist nur innerhalb bestehender Schuldverhältnisse anwendbar.

      Entweder Vertrag, oder auch vorvertragliches Schuldverhältnis.

   2. Erfüllungsgehilfeneigenschaft

      > Erfüllungsgehilfe ist, wer im Pflichtenkreis des Schuldners mit dessen Willen für ihn tätig wird.

   3. Verschulden des Erfüllungsgehilfen



Unter **Pflichtverletzung**

1. Eigene Pflichtverletzung des Schuldners (-)

2. Zurechnung der Pflichtverletzung des Erfüllungsgehilfen nach § 278 BGB analog

   Selber Aufbau.

   




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs