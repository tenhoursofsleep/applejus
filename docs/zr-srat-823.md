---
id: zr-srat-823
sidebar_label: Schadensersatz, 823 I
title: Schadensersatz nach § 823 I
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-srat-823.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-srat-823
---



### Aufbau

1. Verletzung eines durch § 823 I BGB geschützten Rechtsguts

2. Verletzungshandlung

   Die Verletzungshandlung kann auch in einem Unterlassen bestehen. Ein Unterlassen steht einem aktiven Tun gleich, wenn eine Pflicht zur Handlung bestand. Eine solche Pflicht kann sich auch aus Vertrag ergeben, die dann gleichzeitig deliktische Garantenpflicht ist. 

3. Haftungsbegründende Kausalität

   Verletzungshandlung muss hinreichend kausal für die Rechtsgutverletzung gewesen sein.

   *Conditio-sine-qua-non*, aber auch objektive Zurechenbarkeit. Rechtsgutverletzung durfte nicht außerhalb des allgemeinen Lebensrisikos liegen.

4. Rechtswidrigkeit

   Durch Rechtsgutverletzung indiziert.

5. Verschulden

   Vorsatz oder Fahrlässigkeit.

6. Haftungsausfüllend kausaler Schaden



### Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs