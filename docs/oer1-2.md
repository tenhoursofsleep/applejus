---
id: oer1-2
title: II. Die Rechtfertigung bei Eingriffe der Judikative
sidebar_label: II. Eingriffe der Judikative
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer1-2.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer1-2
---

2. Begründetheit

    [...]
    
    3. Rechtfertigung
    
       [...]
    
       1. Schranke
    
          [...]
    
       2. Schranken-Schranke
    
          [...]
    
          1. Verfassungsmäßigkeit des Gesetzes als Schranke
    
             [...]
    
          2. Verfassungsmäßige Anwendung im Einzelfall
    
             1. Prüfungsmaßstab <a name="judikative-keine-superrevision"></a>
    
                > Das Bundesverfassungsgericht ist keine "Superrevisionsinstanz". Ihm obliegt lediglich die Prüfung spezifischen Verfassungsrechts, nicht jedoch die Anwendung von einfachem Recht, die Sache der Fachgerichte ist. Demnach ist die Verfassungsbeschwerde dann erfolgreich, wenn das Fachgericht in seiner Rechtsanwendung die Bedeutung und Tragweite der Grundrechte verkannt hat. 
    
             2. *Konkrete Prüfung, insb. Verhältnismäßigkeit*
             

### Quellen
[^1]: Fall 1, GK ÖR SS 2020 
[^2]: Classen, Grundrechte 
[^3]: Fall 3, GK ÖR SS 2020 
[^4]: Skript 