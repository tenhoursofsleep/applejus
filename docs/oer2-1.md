---
id: oer2-1
title: I. Schutzbereich, Eingriff
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer2-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer2-1
---



Über den Schutzbereich des "Grundrechts auf Menschenwürde" besteht keine Einigkeit. Eine vollständige, abstrakte Definition gibt es nicht.



### Persönlicher Schutzbereich

Grundsätzlich jede natürliche Person.



### Eingriff in den (sachlichen) Schutzbereich

Das BVerfG stellt häufig auf die sog. "Objektformel" ab, die eine Art Indizwirkung entfaltet:

> Die Menschenwürde [als solche] ist getroffen, wenn der konkrete Mensch zum Objekt, zu einem bloßen Mittel, zur vertretbaren Größe herabgewürdigt wird.
>
> -- Günter Dürig

> Die Menschenwürde ist aber nicht schon dann angetastet, wenn ein Mensch als Objekt behandelt wird, sondern erst dann, wenn er zum ***bloßen* Objekt** gemacht wird. Das heißt, dass seine **Subjektqualität** durch die Behandlung ***grundsätzlich* in Frage gestellt** wird. "Das ist [insb.] der Fall, wenn die Behandlung durch die öffentliche Gewalt **die Achtung des Wertes vermissen lässt, der jeden Menschen um seiner selbst Willen zukommt**."[^Gr.Lauschangriff]

>Hingegen wird die Menschenwürde nicht angetastet, solange die faktische Objektstellung dergestalt hinreichend **kompensiert** wird, dass der Betroffene noch **in seiner Objektstellung** **Anschluss zu seinem Selbstverständnis als Subjekt finden kann.[^Skript]** 
>
>Solche Kompensationen können zB sein: Die [Einwilligung](oer2-3#schutz-des-menschen-vor-sich-selbst), die persönliche Zurechnung von Handlungsfolgen, das Günstigkeitsprinzip. 



:::note Hinweis zum Aufbau 

Bei der Menschenwürdegarantie fallen die Punkte "Eingriff" und "sachlicher Schutzbereich" zusammen, soweit die Objektformel herangezogen wird. Dies ist alleine der Formulierung der Objektformel geschuldet, da sie gleichzeitig beide dogmatischen Prüfungspunkte enthält[^mM].

:::



### Quellen

[^Skript]: Skript 

[^Gr.Lauschangriff]: Großer Lauschangriff, BVerfGE 109, 279

[^mM]: Meine Meinung :)