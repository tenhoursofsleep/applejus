---
id: oer-allgemeines-drittwirkung
title: VII. Mittelbare Drittwirkung der Grundrechte
sidebar_label: VII. Mittelbare Drittwirkung
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-allgemeines-drittwirkung.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-allgemeines-drittwirkung
---

*Noch kein Inhalt*


### Quellen
[^1]: Fall 1, GK ÖR SS 2020 
[^2]: Classen, Grundrechte 
[^3]: Fall 3, GK ÖR SS 2020 
[^4]: Skript 