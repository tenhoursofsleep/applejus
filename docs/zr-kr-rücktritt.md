---
id: zr-kr-rücktritt
sidebar_label: Rücktritt wegen Mängel
title: Rücktritt wegen Mängel
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-kr-rücktritt.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-kr-rücktritt
---



## Fallunterscheidung

- Rücktritt nach §§ 437 Nr. 2, 323 I
- Rücktritt nach §§ 437 Nr. 2, 326 V, 323



## 437 Nr. 2, 323 I

1. Anspruch aus §§ 346 I, 323 I, 437 Nr. 2 Alt. 1, 434, 433 BGB
   1. Rücktrittsgrund
      1. Kaufvertrag
      2. Mangel bei Gefahrübergang
      3. Fristsetzung
      4. Kein Ausschluss
         1. Erheblichkeit, 323 V
         2. Verantwortlichkeit des Gläubigers, 323 VI
   2. Rücktrittserklärung
   3. Ergebnis



## 437 Nr. 2, 326 V, 323

1. Anspruch aus §§ 346 I, 323 I, 326 V, 437 Nr. 2 Alt. 1, 434, 433 BGB
   1. Rücktrittsgrund
      1. Kaufvertrag
      2. Mangel bei Gefahrübergang
      3. Entbehrlichkeit der Fristsetzung
      4. Kein Ausschluss
         1. Erheblichkeit, 323 V
         2. Verantwortlichkeit des Gläubigers, 323 VI
   2. Rücktrittserklärung
   3. Ergebnis






## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs