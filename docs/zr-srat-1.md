---
id: zr-srat-1
sidebar_label: Überblick
title: Überblick Schuldrecht AT
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-srat-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-srat-1
---

## Vertragliche, Vertragsähnliche Schuldverhältnisse

### Ansprüche

##### Erfüllung

- Naturalerfüllung, § 241 I

#### Schadensersatz

- Schadensersatz nach 280 I (pur); 280 I, 241 II

- Schadensersatz statt der Leistung, §§ 280 I, III, 281-283

  - Ersatz vergeblicher Aufwendungen, §§ 284, 280 I, III, 281-283
  - [Herausgabe des stellvertretenden commodum, § 285](zr-srat-285)

- Ersatz von Verzögerungsschäden, §§ 280 I, II, 286

  - Verzugszinsen, §§ 288, 286

  

#### Rücktritt

- Herausgabe des Empfangenen und der Nutzungen (oder Wertersatz), 346 I/II wegen

	- Rücktritt: 323 I
	- Rücktritt: 326 V, 323 
	- Rücktritt wegen Verletzung einer Schutzpflicht: 324
	- Ausschluss der Leistungspflicht : 326 I 1, IV


### Einwendungen, Einreden

#### Unmöglichkeit und Unzumutbarkeit, § 275 

- Unmöglichkeit, 275 I
- Grobes Missverhältnis zu Leistungsinteresse, 275 II
- Unzumutbare persönliche Leistung, 275 III

#### Einreden bei Synallagma

- Einrede des nicht erfüllten Vertrags, 320 I
- Unsicherheitseinrede, 321 I 1



### Besondere Konstellationen

- [Haftung für Erfüllungsgehilfe, § 278](zr-srat-278)

- Wegfall der Geschäftsgrundlage, § 313
- Vertrag mit Schutzwirkung zugunsten Dritter
- [Drittschadensliquidation](zr-srat-dsl)
- Gesamtschuldner



### Sonstige Themen

- Annahmeverzug, 293 ff

- Schuldnerverzug, 286 ff

- Aufrechnung, 387 ff

- Abtretung, 398 ff

- Schuldübernahme, 414 ff

- Verbraucherwiderruf

- AGB-Kontrolle

- Erfüllungsmodalitäten, u.a. 362 ff


## Deliktische Ansprüche

- [Schadensersatz nach § 823 I](zr-srat-823)

  




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs