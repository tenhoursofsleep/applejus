---
id: oer-versammlungsfreiheit-1
title: I. Schutzbereich
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-versammlungsfreiheit-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-versammlungsfreiheit-1
---

## Persönlicher Schutzbereich

Die Versammlungsfreiheit, Art. 8 GG, ist ein **Deutschengrundrecht**. 

:::note Definition
**Deutscher** ist, wer die deutsche Staatsangehörigkeit besitzt, Art. 116 I GG.
:::

:::info Art. 18 AEUV

Das Diskriminierungsverbot nach Art. 18 I AEUV nicht vergessen!

::: 



## Sachlicher Schutzbereich

Nach Art. 8 I GG haben alle Deutsche das Recht, sich ohne Anmeldung oder Erlaubnis **friedlich** und **ohne Waffen** zu **versammeln.**

### 1. Versammlung

:::note Definition

Eine Versammlung ist eine **Zusammenkunft mehrerer Menschen** zu einem **gemeinsamen Zweck**.

:::

#### Zusammenkunft Mehrerer Menschen

Nach hM sind hierfür bereits **zwei Menschen** hinreichend.

Es gibt auch aA, die stattdessen anhand zivilrechtlicher Regeln die Grenze bei 3 oder 7 Menschen sehen.

#### Gemeinsamer Zweck

Die Versammlungsteilnehmer müssen durch eine **gemeinsame Zwecksetzung** innerlich verbunden sein[^Skript]. Nicht ausreichend ist die bloße, zufällige Verfolgung des *gleichen* Zwecks. 

Nach der neuen Rspr. muss zudem dieser Zweck in der **Kundgebung oder gemeinsamen Erörterung öffentlicher bzw. politischer Angelegenheiten bestehen **(sehr umstritten). Demnach kann nur die spezielle Bedeutung kollektiver Meinungsäußerungen für den demokratischen Prozess den erhöhten Schutz des Art. 8 als "dienende Freiheit" begründen[^Fall-8].

Das bayerische Versammlungsgesetz fasst es anlehnend an dieser Rspr. wie folgt:

> Eine Versammlung ist eine Zusammenkunft von mindestens zwei Personen zur gemeinschaftlichen, überwiegend auf die Teilhabe an der öffentlichen Meinungsbildung gerichteten Erörterung oder Kundgebung.

### 2. Ohne Waffen

:::note Definition

Zu dem Begriff der „Waffen“ gehören jedenfalls Waffen i.S. des WaffG, aber auch sonstige Gegenstände, die **objektiv** zur Verletzung von Personen oder zur Sachbeschädigung geeignet sind **und** **subjektiv** zu diesem Zweck mitgeführt werden.

:::

Hierzu zählen reine **Schutzgegenstände**, zB Schutzbrillen, Gasmasken, etc., nicht.

### 3. Friedlich

Einer Ansicht nach ist eine Versammlung schon dann unfriedlich, wenn ein unbefangener Beobachter ihr eine **Drohung mit Gewalt** entnehmen würde. Dies ist nach hM abzulehnen, da Versammlungen gerade einen wesentlichen Teil der Effektivität ihrer Einwirkung auf die Willensbildung Dritter durch die Demonstration von "Macht durch Masse" und durch Behinderungen zB des Verkehrs erzielen. Diese sind also durch Art. 8 geschützte Verhaltensweisen. Es reicht also nicht jede Form von Gewalt im strafrechtlichen Sinne, also jede körperliche Zwangswirkung beim Opfer, aus. Dies widerspräche das Wesen des durch Art. 8 geschützten Verhaltens. 

Darüber hinaus spricht auch die systematische Gleichstellung von "friedlich" und "ohne Waffen" dafür, vergleichsweise ähnlich weite Grenzen bei der Auslegung dieser Begriffe zu ziehen. Ein weites Verständnis der Unfriedlichkeit würde das Kriterium "ohne Waffen" sinnlos werden.

:::note Definition (hM)

Eine Versammlung ist erst dann **unfriedlich**, wenn Handlungen von einiger Gefährlichkeit stattfinden. Sie muss also einen **gewalttätigen oder aufrührerischen Verlauf** nehmen, oder dieser muss unmittelbar bevorstehen[^Fall-8].

:::



### Abgrenzung zur Meinungsfreiheit

Nach BVerfG schützt die Versammlungsfreiheit die **äußere Gestaltung** einer Versammlung, also **Ort, Zeit, etc.,** während der **Inhalt der Aussagen** durch die Meinungsfreiheit, Art. 5 I 1 GG geschützt ist.



### Schutz begleitender Tätigkeiten

Der Schutzbereich ist weit zu verstehen. Geschützt sind insbesondere auch:

- Die Vorbereitung der Versammlung
- Die Anfahrt, das Zusammenkommen
- Die Abfahrt



## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs