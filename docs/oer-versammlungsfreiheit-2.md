---
id: oer-versammlungsfreiheit-2
title: II. Eingriff
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-versammlungsfreiheit-2.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-versammlungsfreiheit-2
---

Der Eingriff richtet sich nach den allgemeinen Regeln.

Insbesondere stellen Eingriffe dar:

- Anmeldepflicht
- Erlaubnispflicht
- Auflagen
- Auflösung
- **Observation**




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs