---
id: zr-kr-mangel
sidebar_label: Der Mangelbegriff
title: Der Mangelbegriff
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-kr-mangel.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-kr-mangel
---

## Sachmangel

:::note Definition

Ein **Sachmangel** ist eine negative Abweichung des Ist-Zustands vom Soll-Zustand (im Zeitpunkt des Gefahrübergangs).

:::

Zunächst:

> Die Kaufsache ist frei von Sachmängel, wenn sie die **vereinbarte Beschaffenheit** hat. (§ 434 I 1)

:::note Definition

Die **Beschaffenheit** einer Sache umfasst alle **physisch anhaftende Eigenschaften** der Sache sowie andere Umstände, die einen hinreichenden Bezug zur Sache haben, insb. die **Beziehungen der Sache zur Umwelt**.

:::

Wenn **keine Beschaffenheitsvereinbarung** vorliegt:

> Die Kaufsache ist sodann frei von Sachmängel, wenn sie sich für die **vertraglich vorausgesetzte Verwendung** eignet. (§ 434 I 2 Nr. 1)

Str., ob die Verwendung **vereinbart** sein muss oder nur beidseitig unterstellt. Rspr: letzteres.



Wenn auch **keine vorausgesetzte Verwendung**:

> Die Kaufsache ist zuletzt frei von Sachmängel, wenn sie die **übliche Beschaffenheit** aufweist und sich für die **gewöhnliche Verwendung** eignet.



### Verbrauchsgüterkauf

Bei Verbrauchsgüterkäufe gilt nach § 477 eine widerlegliche Vermutung, dass eine Sache schon bei Gefahrübergang mangelhaft war, falls ein Sachmangel sich **innerhalb von 6 Monaten** zeigt. Die Vermutung kann sich auch auf den **Grundmangel** beziehen, also nicht zwangsläufig denselben Mangel, der sich dann zeigt. 



## Rechtsmangel

- Beschränkt dingliche Rechte
- Obligatorische Rechte, wenn sie gegenüber Erwerber Wirkung entfalten.




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs