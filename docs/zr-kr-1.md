---
id: zr-kr-1
sidebar_label: Überblick
title: Überblick Kaufrecht
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-kr-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-kr-1
---



## Nichtleistung

- Anspruch auf Erfüllung
- Rücktritt
- Schadensersatz neben der Leistung (Verzögerungsschäden)
- Schadensersatz statt der Leistung





## Schlechtleistung (Mängelgewährleistung)

- Nacherfüllung, §§ 439 I, 437 Nr. 1
- Rücktritt, §§ 437 Nr. 2, 440, 323/326 V
- Minderung, §§ 437 Nr. 2, 441
- Schadens/Aufwendungsersatz statt oder neben der Leistung, §§ 437 Nr. 3, 280 I, III, 281/283, oder 311a 



### Zeitliche Abgrenzung zwischen Nichtleistung und Schlechtleistung

Nach hM kommt es auf den Gefahrübergang an, 446 f; a.A. Annahme als Erfüllung, 326



### Gemeinsame Themen

- Mangelbegriff, 434 f.
- Verjährung, 438.
- Garantie
- Verbrauchsgüterkauf




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs