---
id: oer2-3
title: III. Einzelne Probleme
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer2-3.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer2-3
---

### Rechtsnatur der Menschenwürdegarantie

### Würde gegen Würde abwiegen?

### Schutz des Menschen vor sich selbst

### Quellen

[^Skript]: Skript 

[^Gr.Lauschangriff]: Großer Lauschangriff, BVerfGE 109, 279

[^mM]: Meine Meinung :)