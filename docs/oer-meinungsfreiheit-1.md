---
id: oer-meinungsfreiheit-1
title: I. Schutzbereich, Eingriff
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-meinungsfreiheit-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-meinungsfreiheit-1
---



## Sachlicher Schutzbereich

### Werturteile

Unstreitig sind von der Gewährleistung des Art. 5 Abs. 1 S. 1 GG sog. **Werturteile** erfasst. Diese sind durch ein Element der Stellungnahme und des Dafürhaltens im Rahmen einer geistigen Auseinandersetzung gekennzeichnet und keinem Wahrheitsbeweis zugänglich.

### Tatsachenbehauptungen

Hierunter werden Behauptungen über Vorgänge der äußeren oder inneren Lebenswelt verstanden, die prinzipiell dem Beweis zugänglich sind.

Es ist umstritten, ob diese unter dem Schutzbereich des Art. 5 Abs. 1 S. 1 GG fallen. Aber Tatsachenbehauptungen und Werturteile sind naturgemäß miteinander verknüpft, da Tatsachenbehauptungen oft notwendige Bedingung für das Bilden einer Meinung ist. Eine trennscharfe Unterscheidung ist auch oft nicht möglich. Daher genießen Tatsachenbehauptungen auch den Schutz der Meinungsfreiheit.

Ausnahme: Tatsachenbehauptungen, die nichts zur Meinungsbildung beitragen können: **Bewusst** unwahre Tatsachenbehauptungen, **erwiesen** unwahre Tatsachenbehauptungen und sog. „**bloße**“ Tatsachenbehauptungen, die nichts zu einer Meinungsbildung beitragen können (wie etwa rein statistische Angaben).



### Kundgabemodalitäten

Die Aufzählung "Wort, Schrift, und Bild" ist nicht abschließend.




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs

