---
id: oer0
title: Inhaltsverzeichnis
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer0.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer0
---


#### § 1 Allgemeines
- I. Verfassungsbeschwerde
- II. Die Verhältnismäßigkeit
- III. Verfassungsmäßigkeit eines Gesetzes
- IV. Die Rechtfertigung bei Eingriffe der Judikative
- V. Die Rechtfertigung bei Eingriffe der Exekutive
- VI. Die Rechtfertigung bei Eingriffe der Legislative
- VII. Wesentlichkeitstheorie



#### § 2 Menschenwürde, Art. 1 GG




#### § 3 Allgemeine Handlungsfreiheit, Art 2 I GG


#### § 4 Allgemeines Persönlichkeitsrecht, Art 2 I iVm Art 1 I GG



#### § 5 Körperliche Unversehrtheit, Art 2 II GG



#### § 6 Gleichheit, Art 3 GG



#### § 7 Glaubens-, Gewissens- und Religionsfreiheit, Art. 4 GG



#### § 8 Meinungsfreiheit, Art. 5 I, II GG



#### § 9 Freiheit der Kunst und der Wissenschaft, Art. 5 III GG



#### § 10 Versammlungsfreiheit, Art. 8 GG



#### § 11 Vereinigungsfreiheit, Art. 9 GG



#### § 12 Brief-, Post-, Fernmeldegeheimnis, Art. 10 GG
