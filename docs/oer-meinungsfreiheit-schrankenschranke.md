---
id: oer-meinungsfreiheit-schrankenschranke
title: III. Schranken-Schranke
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-meinungsfreiheit-schrankenschranke/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-meinungsfreiheit-schrankenschranke
---



### Wechselwirkungslehre

"Dabei ist zu beachten, dass allein das Vorliegen eines allgemeinen Gesetzes – hier §§ 1004, 823 BGB – nicht dazu führt, dass der Grundrechtsein- griff gerechtfertigt ist. Vielmehr müssen nach der sog. **Wechselwirkungslehre** die allgemeinen Gesetze ihrerseits wiederum im Lichte der besonderen Bedeutung der Meinungsfreiheit für den freiheitlich-demokratischen Rechtsstaat (einschränkend) ausgelegt werden (Konkretisierung der allgemeinen praktischen Konkordanz)."

-> Einschüchterungseffekt



### Aufbau

- Inhalt der Aussage
- Abwägung
  - Maßstab bei Mehrdeutigkeit




## Quellen

Fall 5

