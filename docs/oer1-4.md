---
id: oer1-4
title: IV. Die Rechtfertigung bei Eingriffe der Legislative
sidebar_label: IV. Eingriffe der Legislative
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer1-4.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer1-4
---
*Noch kein Inhalt*

### Quellen
[^1]: Fall 1, GK ÖR SS 2020 
[^2]: Classen, Grundrechte 
[^3]: Fall 3, GK ÖR SS 2020 
[^4]: Skript 