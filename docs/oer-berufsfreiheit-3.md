---
id: oer-berufsfreiheit-3
title: III. Rechtfertigung
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-berufsfreiheit-3.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-berufsfreiheit-3
---

## Schranke

> Das Grundrecht der Berufsfreiheit steht unter einem ausdrücklichen **Regelungsvorbehalt** (Art. 12 Abs. 1 S. 2 GG). Ihrem Wortlaut nach bezieht sich diese Schrankenregelung zwar nur auf die Berufsausübung. In Konsequenz der Annahme eines **einheitlichen Grundrechts** wird aber auch die Schrankenregelung des Art. 12 Abs. 1 S. 2 GG weit und einheitlich verstanden. Sie gilt demnach ebenso für die Berufswahlfreiheit.

Es gibt prinzipiell einen Unterschied zwischen einem **Regelungsvorbehalt** und dem **Gesetzesvorbehalt** (i.e. **Eingriffsvorbehalt**). Hier ist das aber laut BVerfG **irrelevant**, in der Sache handelt es sich um einen normalen Gesetzesvorbehalt.



## Schranken-Schranke

Prinzipiell sind die Schranken-Schranken wieder: 1. Verfassungsmäßigkeit des Gesetzes, 2. Verfassungsmäßigkeit der Einzelmaßnahme.

Dabei soll die Abwägung aber nach den Grundsätzen der sog. "Drei-Stufen-Lehre" erfolgen.

### Drei-Stufen-Lehre

Das BVerfG entscheidet im Rahmen dieser Theorie zwischen **drei Eingriffstypen**:

**1. Berufsausübungsbeschränkungen**

**2. Subjektive Beschränkungen der Berufswahl**

:::note Definition

**Subjektive Beschränkungen der Berufswahl** binden den Zugang zu einem Beruf an persönliche Eigenschaften oder Fähigkeiten, Kenntnisse oder Erfahrungen, auf deren Erfüllung der Bürger einen Einfluss hat.

:::

**3. Objektive Beschränkungen der Berufswahl**

:::note Definition

**Objektive Beschränkungen der Berufswahl** binden den Zugang zu einem Beruf an objektiver, dem Einfluss des Einzelnen entzogener Kriterien.

:::

Grundsätzlich sind **objektive** Berufswahlbeschränkungen **schwerwiegender** als **subjektive** Berufswahlbeschränkungen, die wiederum **schwerwiegender** als Berufs**ausübungs**beschränkungen sind. Die Rechtfertigungsmaßstäbe sind dann anhand der Schwere zu bemessen.

:::info Aufbau

In der Klausur wird diese Theorie bei der **Verhältnismäßigkeitsprüfung** relevant. Die Theorie wird zweimal angewendet: bei der Erforderlichkeit und bei der Angemessenheit. Es empfehlt sich folgender Aufbau:

>**a. Legitimer Zweck**
>
>**b. Erforderlichkeit**
>
>- Erwähnung der Drei-Stufen-Lehre
>- Feststellung, welche Eingriffsart hier vorliegt
>- Erste Anwendung: Ist eine Maßnahme einer niedrigeren Stufe gleich wirksam, dann ist die angefochtene Maßnahme nicht erforderlich.
>
>**c. Geeignetheit**
>
>**d. Angemessenheit**
>
>- Zweite Anwendung: Beurteilung anhand der Maßstäbe der jeweiligen Stufe.

:::

Die Drei-Stufen-Theorie hat folgende Konsequenzen:

#### 1. Erforderlichkeit

Ist eine Maßnahme einer niedrigeren Stufe gleich wirksam, so ist anzunehmen, dass sie auch milder ist. Damit wäre die angefochtene ursprüngliche Maßnahme nicht erforderlich.

#### 2. Angemessenheit

Bei der Angemessenheit gelten die folgenden Maßstäbe:

> **Berufsausübungsregelungen** sind gerechtfertigt, wenn „**Gesichtspunkte der Zweckmäßigkeit**“ sie verlangen, wobei sich diese Zweckmäßigkeit entweder auf die Gefahren- und Schadensabwehr für die **Allgemeinheit** **oder** auf die Sicherung und Förderung des **Berufsstands** beziehen kann.

> **Subjektive** Zulassungsbeschränkungen sind nur gerechtfertigt, wenn die Ausübung des Berufs ohne die Erfüllung dieser Voraussetzung **„unmöglich oder unsachgemäß wäre**“ oder wenn sie **Gefahren oder Schäden für die Allgemeinheit mit sich brächte**.

> **Objektive** Zulassungsbeschränkungen sind nur gerechtfertigt, wenn sie zur „Abwehr **nachweisbarer** oder **höchstwahrscheinlicher** Gefahren für ein **überragend wichtiges Gemeinschaftsgut**“ notwendig sind.





## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs