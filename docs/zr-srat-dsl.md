---
id: zr-srat-dsl
sidebar_label: Drittschadensliquidation
title: Drittschadensliquidation
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-srat-dsl.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-srat-dsl
---



### Aufbau

Schadensersatzanspruch einer Partei mit verletzter Rechtsstellung?

1. Normale Vorausetzung des Schadensersatzes ...

2. Schaden? (-)

3. Anwendung der Grundsätze der Drittschadensliquidation

   > Fraglich ist, ob hier ein unbilliges Ergebnis vorliegt, das durch die Anwendung der Grundsätze der Drittschadensliquidation korrigiert werden soll.

   1. Inhaber der verletzten Rechtsstellung hat keinen Schaden

   2. Geschädigte hat keinen Anspruch

   3. Zufällige Schadensverlagerung

      Aus einer aus seiner Sicht zufälligen Schadensverlagerung soll der Schädiger keinen Vorteil ziehen können.

   4. Rechtsfolge

      Inhaber der verletzten Rechtsstellung kann Schaden des Geschädigten selbst geltend machen.



### Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs