---
id: oer-versammlungsfreiheit-3
title: III. Rechtfertigung
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-versammlungsfreiheit-3.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-versammlungsfreiheit-3
---

## Schranke

Man muss hierbei zwischen Versammlungen **in geschlossenen Räumen** und Versammlungen **"unter freiem Himmel"** unterscheiden.

Entscheidend hierbei ist **alleine** die Abgrenzung zu den **Seiten** hin (entgegen Wortlaut). Der Grundgesetzgeber wollte mit dieser Regelung eine Kategorie besonders störanfälliger Versammlungen abfangen -- die Störanfälligkeit ergibt sich ja aus der räumlichen Unbegrenztheit, nicht daraus, das der Veranstaltungsort nach oben nicht bedacht ist.

#### Versammlungen unter freiem Himmel

Einfacher Gesetzesvorbehalt.

Teilweise wird hier ein "einschränkendes Gesetz" gefordert, also ein Gesetz, das darauf ausgerichtet ist, Versammlungen zu beschränken. U.a. leite sich hieraus die Polizeifestigkeit des Versammlungsrechts. *Kann ich hier derzeit nicht bestätigen.*

#### Versammlungen in geschlossenen Räumen

Vorbehaltlos garantiert -- nur verfassungsimmanente Schranken.



## Schranken-Schranke

### Verfassungsmäßige gesetzliche Grundlage

Hier wird häufig das Versammlungsgesetz des Bundes oder eines der Länder die notwendige Grundlage sein. Es gibt dazu ein paar Besonderheiten, die man kennen muss.

### Verhältnismäßigkeit des *Gesetzes*

##### Problem Legitimer Zweck: Öffentliche Sicherheit und öffentliche Ordnung

Soweit von öffentlicher Sicherheit oder öffentlicher Ordnung die Rede ist:

:::note Definition

Unter dem Schutz der **öffentlichen Sicherheit** versteht man (im allgemeinen Sicherhheitsrechtlichen Sinne) den Schutz der **Individualrechtsgüter** (insbesondere Unversehrtheit des Lebens, Gesundheit, Ehre, Freiheit und Vermögen), der **Rechtsordnung insgesamt** und der **Einrichtungen des Staates** und sonstiger Träger von Hoheitsgewalt. 

:::

:::note Definition (allgemein)

Unter der **öffentlichen Ordnung** vertesteht man die Gesamtheit der **ungeschriebenen Regeln** über das **Verhalten des Einzelnen** in der Öffentlichkeit, soweit die Beachtung dieser Regeln nach den **herrschenden** **Auffassungen** als unerlässliche Voraussetzung eines geordneten Gemeinschaftslebens betrachtet wird.

:::

Bei der Einstufung der öffentlichen **Sicherheit** als legitimer Zweck gibt es keine Probleme. 

Jedoch kann der Schutz der öffentlichen **Ordnung** **nicht** dahingehend ausgelegt werden, dass schon im Rahmen der Versammlung geäußerte **Meinungen**, die nicht die herrschende entsprechen, eine Einschränkung rechtfertigen können. Das Versammlungsrecht ist wegen ihrer Bedeutung für den demokratischen Prozess gerade ein **Minderheitenrecht**. Nach der Rspr. des BVerfG wäre dies zudem ein Eingriff nicht in die Versammlungsfreiheit, sondern in die Meinungsfreiheit. Würde man den Schutz der öffentlichen Ordnung als eine Regelung, die eine Einschränkung nicht-herrschender Meinungen erlaubt, wäre die Regelung aber gerade kein **allgemeines Gesetz**, so dass die Rechtfertigung dort scheitert.

Der Begriff der öffentlichen **Ordnung** muss aber **einschränkend** verfassungskonform ausgelegt werden, so dass die Rechtfertigung einer Beschränkung nur aufgrund **besonder provokativer oder aggressiver**, das Zusammenleben der Bürger **konkret** beeinträchtigende **Begleitumstände** möglich ist.

-> Mit dieser verfassungskonformen Auslegung ist die öffentliche Ordnung also auch ein **legitimer Zweck**.



### Bestimmtheitsgrundsatz bzgl. öffentlicher Ordnung (*Gesetz*)

Der Bestimmtheitsgrundsatz bezüglich einer generalklauselartige Einschränkungsbefugnis zum Schutze der öffentlichen Ordnung ist diskutierbar.



### Erlaubnis, Anmeldung?

Eine Erlaubnispflicht ist in Deutschland nirgendwo vorgesehen, Anmeldepflichten jedoch schon. -> *Problem*



### Verfassungsmäßigkeit im *Einzelfall*

Hier soll anhand der allgemeinen Regeln insb. die Verhältnismäßigkeit geprüft werden. Beachte die Besonderheiten bei jeder Form der Verfassungsbeschwerde (also gegen Judikative, Exekutive, oder Legislative).

Zur Orientierung können grundsätzliche Bespiele dienen, die jedoch in der Klausur nochmals hergeleitet werden müssen:

- Die öffentliche Ordnung kann nur für Auflagen aber nicht für Versammlungsverbote herangezogen werden.




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs