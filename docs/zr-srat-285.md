---
id: zr-srat-285
sidebar_label: Stellvertretendes commodum, 285
title: Ersatz des stellvertretenden commodum, § 285
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-srat-285.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-srat-285
---

### Aufbau

1. Wegfall der Leistungspflicht

2. Erlangung eines Surrogats infolge der Leistungsbefreiung

   > Der Schuldner müsste kausal wegen der Umstände, die zur Leistungsbefreiung führten, einen Ersatzgegenstand erlangt haben.

3. Identität zwischen Geschuldetem und Ersatz

   > "Zwischen dem Gegenstand, dessen Leistung unmöglich geworden ist, und dem Gegenstand, für den der Schuldner einen Ersatz oder Ersatzanspruch erlangt hat muss Identität bzw. Kongruenz bestehen. Entscheidend ist hier eine rein wirtschaftliche Betrachtungsweise danach, ob der geschuldete Gegenstand und das Surrogat aus der Sicht des Gläubigers funktionell vergleichbar sind."






## Quellen

Fall 6 der AGs