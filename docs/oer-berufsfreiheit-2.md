---
id: oer-berufsfreiheit-2
title: II. Eingriff
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-berufsfreiheit-2.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-berufsfreiheit-2
---



## Klassischer Eingriffsbegriff

Ein Eingriff liegt immer schon vor, wenn die Eingriffstatbestände im klassischen Sinne erfüllt sind.

Insbesondere muss die beanstandete Maßnahme **final** sein, dh. sie müssen sich unmittelbar auf die berufliche Betätigung richten.



## Moderner Eingriffsbegriff

### Berufsregelnde Tendenz

Soweit nur eine Maßnahme nur faktisch-mittelbare Auswirkungen auf die Berufsfreiheit entfalten, verlangt das BVerfG **zusätzlich**, dass der Eingriff eine **objektiv "berufsregelnde Tendenz"** aufweisen muss. Dies ist der Fall, falls ein **enger Zusammenhang** der Maßnahme mit der Berufsausübung besteht, oder falls die Maßnahme sich **unmittelbar** auf die Berufsausübung auswirkt, dh sie hat eine vorhersehbare und letztlich in Kauf genommene (schwerwiegende) Beeinträchtigung der Berufsausübung zu Folge.

Dieses Kriterium führt zB dazu, dass allgemeine, berufsneutrale Maßnahmen wie zB das Aufstellen von Verkehrsschildern keine Eingriffe in die Berufsfreiheit (zB eines Anwalts) darstellen. 

Diese Rspr. ist nicht unumstritten.




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs