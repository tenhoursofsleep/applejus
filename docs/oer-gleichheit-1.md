---
id: oer-gleichheit-1
title: I. Allgemeiner Gleichheitssatz
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-gleichheit-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-gleichheit-1
---



1. Ungleichbehandlung

2. Rechtfertigung

   1. Maßstab

      > Dabei ist zunächst zu klären, an welchem Rechtfertigungsmaßstab sich diese Ungleichbehandlung messen lassen muss. 

      **Willkürformel:** Eine Ungleichbehandlung ist gerechtfertigt, wenn sich nur **irgendein sachlicher Grund** für die Unterscheidung anführen lässt. -> Gestaltungsspielraum des Gesetzgebers nicht zu sehr verengen.

      **Neue Formel:** Ungleichbehandlungen sind nur zulässig, wenn Gründe solcher **Art** und von solchem **Gewicht** vorliegen, dass die Ungleichbehandlung **ausnahmsweise** **gerechtfertigt** werden kann

      **Eine Ansich**t: Immer nur Willkürformel.

      **Alte Rspr** des BVerfG: Bei **personenbezogenen** Ungleichbehandlungen immer "neue Formel", bei **verhaltensbezogenen** Ungleichbehandlungen grundsätzlich Willkürformel, aber bei besonders schwerwiegenden Ungleichbehandlungen auch "neue Formel". Dafür hat es 3 "**Je-Desto"-Formeln** aufgestellt:

      - Die Intensität wiegt umso schwerer, je stärker sich das konkrete Differenzierungsmerkmal an die in Art. 3 Abs. 3 GG genannten Kriterien annähert.
      - Die Intensität wiegt umso schwerer, je weniger der Einzelne dieses Kriterium frei beeinflussen kann.
      - Die Intensität wiegt umso schwerer, je stärker das Differenzierungskriterium zu einer Einschränkung von Freiheitsrechten führt.

      **Neue Rspr** des BVerfG: Nach der neuen Rspr des BVerfG gilt es, einen stufenlosen Prüfungsmaßstab der Verhältnismäßigkeit anzustellen, dessen Intensität sich nach den o.g. Je-Desto-Formeln richtet.

      > Der Meinungsstreit kann jedoch dahinstehen, wenn alle Ansätze zu demselben Ergebnis führen.

   2. Willkürformel

   3. "Neue Formel", gleitender Skala

      Verhältnismäßigkeitsprüfung: Hier die Je-Desto-Formeln anwenden.

   4. Ggf. Streitentscheid




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs

