---
id: zr-kr-schadensersatz
sidebar_label: Schadensersatz wegen Mängel
title: Schadensersatz wegen Mängel
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Fzr-kr-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Fzr-kr-1
---



## Fallunterscheidung

- Schadensersatz statt der Leistung
  - Behebbare Mängel
  - Unbehebbare Mängel: §§ 280 I, III, 283, 437 Nr.3 oder §§ 311a, 437 Nr. 3
- Schadensersatz neben der Leistung
  - Mangelfolgeschäden: §§ 280 I, 437 Nr. 3
  - Andere Schäden -> SchR AT




## Schadensersatz statt der Leistung

### Behebbare Mängel

1. Anspruch auf Schadensersatz statt der Leistung aus §§ 280 I, III, 281, 437 Nr. 3, 434, 433 BGB

   1. Pflichtverletzung, (II.) Vertretenmüssen

      Entweder: Verletzung der Pflicht aus § 433 I 2 zur **mangelfreien Leistung** -> Verschulden liegt in Verursachung des Mangels oder Nichtbehebung trotz Kenntnis

      Oder: Verletzung der **Nacherfüllungspflicht**

      Eins von beiden reicht.

   2. Fristsetzung

   3. Bei Schadensersatz statt der *ganzen* Leistung: Erheblichkeit

   4. Kausaler Schaden



### Unbehebbare Mängel

1. Anspruch auf Schadensersatz statt der Leistung aus §§ 280 I, III, 283, 437 Nr. 3, 434, 433 BGB (nachträgliche Unmöglichkeit)

   1. Pflichverletzung, (II.) Vertretenmüssen 

      Jedenfalls (+), wenn V die Umstände, die zu Unmöglichkeit führten, zu vertreten hat. Dann Pflichtverletzung = Unbehebbarkeit bzw. Nichtnacherfüllung

      Str., ob auch Pflichtverletzung = mangelhafte Leistung möglich ist. (Meiner Meinung nach (+))

   2. Bei Schadensersatz statt der *ganzen* Leistung: Erheblichkeit

   3. Kausaler Schaden



### Kleiner vs. Großer Schadensersatz

- Kleiner Schadensersatz: Käufer behält Sache, ersetzt wird Differenz zu dem Zustand, der bei ordnungsgemäßer Erfüllung bestünde.
- Großer Schadensersatz: Käufer gibt Sache zurück, das volle Äquivalenzinteresse wird liquidiert.



## Schadensersatz neben der Leistung

- Die Anspruchsgrundlage bei mangelbezogenen Pflichtverletzungen immer mit § 437 Nr. 3!
- Bei anfänglicher Unmöglichkeit: 311a, umfasst auch SE neben der Leistung
- Verzögerungsschaden auch möglich
- Nutzungsausfallschaden nach hM 280 I pur, nicht 286.




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs