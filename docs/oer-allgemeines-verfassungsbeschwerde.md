---
id: oer-allgemeines-verfassungsbeschwerde
title: I. Verfassungsbeschwerde
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-allgemeines-verfassungsbeschwerde.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-allgemeines-verfassungsbeschwerde
---

### Kurzschema

1. Zulässigkeit
   1. Zuständigkeit
   2. Beschwerdefähigkeit
   3. Verfahrensfähigkeit
   4. Beschwerdegegenstand
   5. Beschwerdebefugnis
      1. Mögliche Grundrechtsverletzung
      2. Selbst, gegenwärtig, und unmittelbar betroffen
   6. Rechtsschutzbedürfnis, Subsidiarität
      1. Rechtswegerschöpfung
      2. Weitere Erfordernisse
   7. Form, Frist
2. Begründetheit
   1. Schutzbereich
      1. Persönlicher Schutzbereich
      2. Sachlicher Schutzbereich
   2. Eingriff
   3. Rechtfertigung
      1. Schranke
      2. Schranken-Schranke



------


### Detailliertes Schema

> Die Verfassungsbeschwerde hat Erfolg, soweit sie zulässig und begründet ist.

1. Zulässigkeit

   1. Zuständigkeit

      > Die Zuständigkeit des Bundesverfassungsgerichts ergibt sich aus Art. 93 Abs.1 Nr. 4a GG, § 13 Nr. 8a, §§ 90 ff. BVerfGG. [^1]

   2. Beschwerdefähigkeit

      > A müsste zunächst gem. § 90 Abs. 1 BVerfGG beschwerdefähig sein. Beschwerdefähig ist nach dieser Vorschrift „jedermann“, d.h. jeder Grundrechtsträger. [^1]

   3. Verfahrensfähigkeit

      > Darüber hinaus müsste A auch prozessfähig sein. [^1]

      Die Prozessfähigkeit ist nicht im BVerfGG geregelt. Sie bezeichnet die Fähigkeit, Prozesshandlungen selbst oder durch Bevollmächtigte vor- zunehmen, und folgt der Grundrechtsmündigkeit. [^1]

   4. Beschwerdegegenstand

      > Es müsste auch ein tauglicher Beschwerdegegenstand vorliegen. Gegenstand der Verfassungsbe- schwerde kann gem. § 90 Abs. 1 BVerfGG jeder Akt der öffentlichen Gewalt sein, d.h. jede Maßnahme oder Unterlassung von Legislative, Exekutive oder Judikative. [^1]

   5. Beschwerdebefugnis

      > Schließlich müsste A auch beschwerdebefugt sein, d.h. eine Grundrechtsverletzung plausibel geltend machen. Die geltend gemachte Grundrechtsverletzung müsste A auch selbst, gegenwärtig und unmittelbar betreffen. [^1]

      1. Mögliche Grundrechtsverletzung

         > Die behauptete Grundrechtsverletzung müsste möglich, d.h. nicht von vorneherein unter jedem denkbaren Gesichtspunkt ausgeschlossen sein. [^1]

      2. Selbst, gegenwärtig, und unmittelbar betroffen

        **Selbst betroffen** ist derjenige, der sich auf die Verletzung eigener grundrechtlicher Positionen beruft. [^1]

         **Gegenwärtig** ist die Beschwer, wenn der Beschwerdeführer schon oder noch betroffen ist.[^1]

         Die **Unmittelbarkeit** ist gegeben, falls es für die konkrete Belastung beim Beschwerdeführer keine weiteren Umsetzungsakte bedarf.

   6. Rechtsschutzbedürfnis (Subsidiarität)

      1. Rechtswegerschöpfung

         Soweit gegen den Beschwerdegegenstand ein Rechtsweg gegeben ist, muss dieser im Grundsatz vor Erhebung einer Verfassungsbeschwerde auch ausgeschöpft werden (§ 90 Abs. 2 BVerfGG). [^2]

      2. Andere Erfordernisse der Subsidiarität

         Das BVerfG fordert vom Beschwerdeführer über die Erschöpfung des Rechtsweges hinaus auch, **alle sonstigen, ihm möglichen und zumutbaren Maßnahmen zu ergreifen**, um die geltend gemachte Grundrechtsverletzung ohne Inanspruchnahme des BVerfG zu verhindern oder zu beseitigen. [...] Allerdings handelt es sich hier um ein flexibles Instrument, bei dem der erwähnte Gesichtspunkt der Zumutbarkeit auch zu einzelfallbezogenen Lösungen führen kann. [^2]

   7. Form und Frist

      Die Verfassungsbeschwerde müsste innerhalb der Monatsfrist des § 93 Abs. 1 BVerfGG erhoben werden. Dabei sind die Formvorschriften nach §§ 23 Abs. 1, 92 BVerfGG zu beachten.[^1]

2. Begründetheit (Freiheitsrecht)

   > Die Verfassungsbeschwerde der A ist begründet, wenn sie duch `Maßnahme` in ihren Grundrechte verletzt ist.
   >
   > Dies ist der Fall, wenn sie (`Maßnahme`) in den Schutzbereich eines Grundrechts der A eingreift und dieser Eingriff nicht verfassungsrechtlich gerechtfertigt ist.

   1. Schutzbereich

      > Der Schutzbereich der `Berufsfreiheit` müsste in persönlicher und sachlicher Hinsicht eröffnet sein[^3].

      1. Persönlicher Schutzbereich

         Zu unterscheiden ist u.a. zwischen "Jedermanns-" und "Deutschengrundrechte". Beachte hierbei Art. 18 AEUV.

         Relevant kann auch sein, ob eine juristische Person in den persönlichen Schutzbereich eines Grundrechts fällt.

      2. Sachlicher Schutzbereich

   2. Eingriff

      > Fraglich ist, ob ein Eingriff in das Recht auf `körperliche Unversehrtheit` der A vorliegt[^1].

      Es wird immer zuerst der klassischer Eingriffsbegriff geprüft. Nur wenn dieser nicht einschlägig ist, soll auch der moderne Eingriffsbegriff herangezogen und diskutiert werden.

      1. Klassischer Eingriffsbegriff

         > Nach dem **klassischen Eingriffsbegriff** ist unter einem Eingriff eine Freiheitsverkürzung zu verstehen, die **unmittelbar**, **final**, **imperativ** und **rechtsförmig** erfolgt.
         >
         > Unmittelbar ist eine Freiheits- verkürzung, wenn sie ohne weitere Zwischenschritte die beeinträchtigende Wirkung auslöst. 
         >
         > Die Finalität ist zu bejahen, wenn die Beeinträchtigung bezweckt ist und nicht nur eine reine Neben- folge darstellt. 
         >
         > Die Freiheitsverkürzung ist imperativ, wenn sie ein Ge- oder Verbot enthält, das ggf. mit Befehl und Zwang durchgesetzt werden kann. 
         >
         > Sie ist rechtsförmig, wenn sie durch Rechtsakt, also nicht durch eine bloß tatsächliche Handlung erfolgt.[^1]

      2. Moderner Eingriffsbegriff

         "In der Literatur wird [..] schon seit längerem der sog. „moderne“ Eingriffsbegriff vertreten. Danach ist ein Eingriff **jedes staatliche Handeln, das dem Einzelnen ein Verhalten, das in den Schutzbereich eines Grundrechts fällt, ganz oder teilweise unmöglich macht**, gleichgültig ob diese Wirkung final oder unbeabsichtigt, unmittelbar oder mit- telbar, rechtlich oder tatsächlich (faktisch, informal), mit oder ohne Befehl und Zwang erfolgt.

         Allerdings wird vom Bundesverfassungsgericht nicht jegliche staatliche Maßnahme, die nachteilige mittelbar-faktische Wirkungen auf den grundrechtlichen Schutzbereich erzeugt, als Eingriff in den Schutzbereich qualifiziert. Eine **abstrakte** Definition einer Grundrechtsbeein- trächtigung hat das Bundesverfassungsgericht gleichwohl bisher **nicht** aufgestellt.

         Entscheidend muss demnach für das Vorliegen eines Eingriffs im modernen Sinne sein, ob eine negative Auswirkung auf den Schutzbereich eines Grundrechts auf hoheitliches Handeln zurückzuführen und **dem Staat zurechenbar** ist. Kriterien für diese Zurechnung sind vor al- lem:

         - die Vorhersehbarkeit einer Grundrechtsbeeinträchtigung, und
         - die Schwere einer Grundrechtsbeeinträchtigung." [^4]

      3. Besonderheiten

         Bei manchen Grundrechten gibt es Besonderheiten im Eingriffsbegriff:

         - Art. 12 Berufsfreiheit: "Berufsregelnde Tendenz"

   3. Rechtfertigung

      > Fraglich ist, ob der Eingriff gerechtfertigt ist. Dies ist der Fall, wenn sich der Eingriff auf eine taugliche gesetzliche Grundlage als Schranke stützen kann, und Gesetz und Eingriff die Schranken-Schranken des Grundrechts wahren, die Einzelanordnung also in verfassungsmäßiger Weise von der ihrerseits verfassungsmäßigen gesetzlichen Ermächtigung Gebrauch macht.[^4] *(wenn sich Verfassungsbeschwerde direkt gegen Legislative richtet, gibt es natürlich keine "Einzelanordnung")*

      1. Schranke

         Hier ist zu prüfen, ob eine taugliche Grundrechtsschranke vorliegt.

         Zunächst ist **festzustellen**, auf **welche Weise** das Grundrecht eingeschränkt werden kann. Sodann:

         - Bei Grundrechte mit Gesetzesvorbehalt: Schranke ist ein Parlamentsgesetz. 

           - Dann prüft man, ob das fragliche Gesetz ggf. die **Qualifikationen** des Vorbehalts erfüllt.

         - Bei Grundrechte ohne Gesetzesvorbehalt: Schranke kann nur kollidierendes Verfassungsrecht sein:

           - Es ist festzustellen, dass das Grundrecht trotzdem nicht schrankenlos geschützt ist:

             > Vielmehr findet es seine Schranken in den Grundrechten anderer und in sonstigen Rechtsgüter von Verfassungsrang (verfassungsimmanente Schranken). Dies ergibt sich aus dem Grundsatz der Einheit der Verfassung.

           - Dann sind hier zunächst nur die verfassungsimmanente Schranken zu erläutern.

      2. Schranken-Schranke

         **Falls** das Grundrecht **vorbehaltslos** garantiert ist:

         - > Auch bei der Einschränkung von Grundrechten durch verfassungsimmanente Schranken bedarf es aus Gründen des Gesetzesvorbehalts (Wesentlichkeitstheorie) eine Abwägung in Form eines verfassungsmäßigen Gesetzes.

         1. Verfassungsmäßigkeit des Gesetzes als Schranke *(bei vorbehaltslose GR anders formulieren)*

            1. Formelle Verfassungsmäßigkeit
            2. Materielle Verfassungsmäßigkeit

         2. Verfassungsmäßige Anwendung im Einzelfall (ggf.)

            [*Bei Eingriffe der Judikative: Prüfungsmaßstab*](#judikative-keine-superrevision)

            *Konkrete Prüfung, insb. Verhältnismäßigkeit*



### Quellen
[^1]: Fall 1, GK ÖR SS 2020 
[^2]: Classen, Grundrechte 
[^3]: Fall 3, GK ÖR SS 2020 
[^4]: Skript 