---
id: oer-berufsfreiheit-1
title: I. Schutzbereich
custom_edit_url: https://gitlab.com/tenhoursofsleep/applejus/-/sse/master%2Fdocs%2Foer-berufsfreiheit-1.md/?return_url=https%3A%2F%2Ftenhoursofsleep.gitlab.io%2Fapplejus%2Fdocs%2Foer-berufsfreiheit-1
---

## Sachlicher Schutzbereich

Der Wortlaut des Art. 12 I GG nennt eine Reihe an Schutzbereiche. Entgegen dessen handelt es sich hierbei aber nach Auffassung des BVerfG um einen **einheitlichen Schutzbereich**, da Berufswahl und Berufsausübung nicht klar voneinander abgrenzbar sind (zB im Zeitpunkt der Berufsaufnahme).

Dies führt auch dazu, dass die Schrankenregelung des Art. 12 I 2 auch zumindest formal **einheitlich** ist, also nicht nur für die Berufsausübung gilt.

### Beruf

:::note Definition

Ein Beruf ist jede auf dauer angelegte Tätigkeit, die der Schaffung oder Erhaltung der Lebensgrundlage dient.

:::

Das von manchen vorgeschlagene weitere Kriterium "nicht verboten" wird allgemein abgelehnt. Eine solche Beschränkung würde dazu führen, dass der Gesetzgeber den Schutzbereich des Grundrechts beeinflussen könnte. Das Verbieten eines Berufs soll jedoch gerade von der Berufsfreiheit geschützt werden, so dass es keine Schutzbereichsbeschränkung darstellen kann.



## Persönlicher Schutzbereich

Die Berufsfreiheit ist ein **Deutschengrundrecht**. Art. 18 AEUV beachten.




## Quellen

[^Skript]: Skript 

[^Fall-8]: Fall 8 der AGs